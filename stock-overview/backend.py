import sqlite3
class Database:
    def __init__(self,db):
        self.conn = sqlite3.connect(db)
        self.cur = self.conn.cursor()
        self.cur.execute("CREATE TABLE IF NOT EXISTS stock_overview (id INTEGER PRIMARY KEY, name TEXT, "
                    "price TEXT, exp INTEGER, stocks INTEGER)")
        self.conn.commit()

    def insert(self, name, price, exp, stocks):
        #the NULL parameter is for the auto-incremented id
        self.cur.execute("INSERT INTO stock_overview VALUES(NULL,?,?,?,?)", (name, price, exp, stocks))
        self.conn.commit()


    def view(self):
        self.cur.execute("SELECT * FROM stock_overview")
        rows = self.cur.fetchall()

        return rows

    def search(self,name="", price="", exp="", stocks=""):
        self.cur.execute("SELECT * FROM stock_overview WHERE name = ? OR price = ? OR exp = ? "
                    "OR stocks = ?", (name, price, exp, stocks))
        rows = self.cur.fetchall()
        #conn.close()
        return rows

    def delete(self,id):
        self.cur.execute("DELETE FROM stock_overview WHERE id = ?", (id,))
        self.conn.commit()
        #conn.close()

    def update(self,id, name, price, exp, stocks):
        self.cur.execute("UPDATE stock_overview SET name = ?, price = ?, exp = ?, stocks = ? WHERE id = ?", (name, price, exp, stocks, id))
        self.conn.commit()

    #destructor-->now we close the connection to our database here
    def __del__(self):
        self.conn.close()